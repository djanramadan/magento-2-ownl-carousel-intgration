define([
    'jquery',
    'Cro_SimpleModule/js/owl.carousel.min'
  ], function($){
    'use strict';
    
    $('.owl-carousel').owlCarousel({
        loop:true,
        items: 1,
        nav:true,
        margin:5,
        autoplay:true,
        autoplayTimeout:5000
    });
    
    
  });

